## ams_version=1.0

Model Main_UseCase {
	Section Section_1 {
		StringParameter WorkbookName;
		Procedure ReadFromExcel {
			Body: {
				!if not FileSelect(WorkbookName,extension:".xlsx") then
				!	return;
				!endif;
				
				WorkbookName:= "InitialData.xlsx";
				
				axll::CloseAllWorkBooks;
				axll::OpenWorkBook(WorkbookName);
				
				axll::SelectSheet("Location Type");
				!change P_LocationType to the name of your own parameter
				axll::ReadList(P_LocationType,"A2:B200","C2:C200",ModeForUnknownElements :  1); 
				
				axll::SelectSheet("Distances Between Locations");
				!change P_DistanceBetweenLocations to the name of your own parameter
				axll::ReadTable(P_DistanceBetweenLocations, "A2:A200", "B1:DR1", "B2:DR200", 1);
				
				axll::SelectSheet("Demand");
				!change P_Demand to the name of your own parameter
				axll::ReadList(P_Demand,"A2:A200","B2:B200",ModeForUnknownElements :  1); 
				
				axll::SelectSheet("Supply");
				!change P_Supply to the name of your own parameter
				axll::ReadList(P_Supply,"A2:A100","B2:B100",ModeForUnknownElements :  1); 
				
				axll::SelectSheet("Coordinates");
				
				axll::ReadList(P_Longitude,"A2:A200","B2:B200",ModeForUnknownElements :  1);
				axll::ReadList(P_Latitude,"A2:A200","C2:C200",ModeForUnknownElements :  1);
				
				axll::CloseAllWorkBooks;
			}
		}
	}
	Section DataModel {
		Set Locations {
			Index: l, l2;
		}
		Set TypesOfLocations {
			Index: typ;
		}
		Parameter P_LocationType {
			IndexDomain: (l,typ);
			Range: binary;
		}
		Parameter P_DistanceBetweenLocations {
			IndexDomain: (l,l2);
		}
		Parameter P_Demand {
			IndexDomain: (l);
		}
		Parameter P_Supply {
			IndexDomain: l;
		}
		Parameter P_Longitude {
			IndexDomain: l;
		}
		Parameter P_Latitude {
			IndexDomain: l;
		}
	}
	Section MathModel {
		Variable V_T {
			IndexDomain: (l,l2) | P_DomainCondition(l, l2) and l<>l2;
			Range: nonnegative;
		}
		Parameter P_DomainCondition {
			IndexDomain: (l,l2);
			Definition: {
				if P_LocationType(l,'Factory') and P_LocationType(l2, 'NDC') 
				or 
				P_LocationType(l, 'NDC') and P_LocationType(l2, 'RDC') 
				then
					1
				endif;
			}
		}
		Variable V_Production {
			IndexDomain: l | P_LocationType(l, 'Factory');
			Range: nonnegative;
		}
		Constraint NodeEquilibrum {
			IndexDomain: l;
			Definition: V_Production(l) + sum(l2,V_T(l2, l)) = sum(l2,V_T(l, l2)) + P_Demand(l);
		}
		Variable V_ObjectiveFunction {
			Range: free;
			Definition: sum((l,l2),V_T(l, l2)*P_DistanceBetweenLocations(l, l2));
		}
		MathematicalProgram OptimizeNetwork {
			Objective: V_ObjectiveFunction;
			Direction: minimize;
			Constraints: AllConstraints;
			Variables: AllVariables;
			Type: Automatic;
		}
	}
	Section WebUI_Support {
		StringParameter ps_action {
			IndexDomain: (webui::indexPageActionSpec);
		}
		StringParameter sp_2action {
			IndexDomain: (webui::indexPageExtension,webui::indexPageActionSpec);
		}
		Procedure SaveCase;
	}
	Section Scenarios {
		Set Scenario {
			SubsetOf: Integers;
			Index: s;
		}
		Parameter P_ScenarioObjectiveFunction {
			IndexDomain: s;
		}
		Parameter P_initialDemand {
			IndexDomain: l;
		}
	}
	Procedure MainInitialization {
		Comment: "Add initialization statements here that do NOT require any library being initialized already.";
	}
	Procedure PostMainInitialization {
		Comment: {
			"Add initialization statements here that require that the libraries are already initialized properly,
			or add statements that require the Data Management module to be initialized."
		}
	}
	Procedure MainExecution {
		Body: {
			solve OptimizeNetwork;
		}
	}
	Procedure Scenario_run {
		Body: {
			Scenario := {1..10};
			ReadFromExcel;
			for s do
			
				P_initialDemand(l) := P_Demand(l);
				P_Demand(l) := P_Demand(l)*(1 + Uniform(-0.1,0.1));
			
				solve OptimizeNetwork;
			
				P_ScenarioObjectiveFunction(s) := V_ObjectiveFunction;
				P_Demand(l):= P_initialDemand(l);
			
			endfor;
		}
	}
	Procedure PreMainTermination {
		Body: {
			return DataManagementExit();
		}
		Comment: {
			"Add termination statements here that require all libraries to be still alive.
			Return 1 if you allow the termination sequence to continue.
			Return 0 if you want to cancel the termination sequence."
		}
	}
	Procedure MainTermination {
		Body: {
			return 1;
		}
		Comment: {
			"Add termination statements here that do not require all libraries to be still alive.
			Return 1 to allow the termination sequence to continue.
			Return 0 if you want to cancel the termination sequence.
			It is recommended to only use the procedure PreMainTermination to cancel the termination sequence and let this procedure always return 1."
		}
	}
	Section Quantities_and_Units {
		Comment: {
			"This section contains all the units and quantities that are added automatically by AIMMS.
			It is recommended to declare all your quantities here."
		}
		Quantity SI_Time_Duration {
			BaseUnit: s;
			Conversions: minute->s : #-># * 60;
			Comment: {
				"Expresses the value for the duration of periods.
				
				The unit s has been added automatically because it is a required unit for AimmsWebUI.
				
				The unit minute has been added automatically because it is a required unit for AimmsWebUI."
			}
		}
	}
}
