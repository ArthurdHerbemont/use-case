# Use case

This AIMMS model is a network design example

# install

Please download AIMMS from https://licensing.cloud.aimms.com/license/community.htm

To run this project, you need an AIMMS Version > 4.78

# run

You may run the `MainExection` Procedure and check the results in `V_T`
